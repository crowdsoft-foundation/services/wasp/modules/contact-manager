from config import Config
from sqlalchemy import inspect
from sqlalchemy.orm import scoped_session

class DbEntity():
    session = Config.db_session

    def __init__(self):
        pass

    def toDict(self):
        return {c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs}

    def save(self):
        self.session.add(self)
        self.session.commit()

    def add(self):
        self.session.add(self)

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()


class Migrate():

    def __init__(self):
        self.session = scoped_session(Config.db_session)

    def __exit__(self, exc_type, exc_value, traceback):
        self.session.close()

    def add_column(self, table_name: str, column_name: str, data_type: str, default=None):

        ret = False

        if default is not None:
            try:
                float(default)
                ddl = ("ALTER TABLE {table_name} ADD column {column_name} {data_type} DEFAULT {default}")
            except:
                ddl = ("ALTER TABLE {table_name} ADD column {column_name} {data_type} DEFAULT '{default}'")
        else:
            ddl = ("ALTER TABLE {table_name} ADD column {column_name} {data_type}")

        sql_command = ddl.format(table_name=table_name, column_name=column_name, data_type=data_type,
                                 default=default)
        try:
            self.session.execute(sql_command)
            self.session.commit()
            ret = True
            print("Column '" + column_name + "' was added to table '" + table_name + "'")
        except Exception as e:
            (code, errormsg) = e.orig.args
            if code == 1060:
                print("Column '" + column_name + "' already exists in table '" + table_name + "'")
            else:
                raise
            ret = False
        return ret

    def execute(self, scriptname, sql):
        try:
            rst = self.session.execute(sql)
            self.session.commit()
            print("'" + scriptname + "' successfully performed, " + str(rst.rowcount) + " affected")
        except Exception as e:
            print("'" + scriptname + "' failed")
            print(e)
            ret = False