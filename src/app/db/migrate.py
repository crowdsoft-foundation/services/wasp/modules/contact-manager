from db.abstract import Migrate
from sqlalchemy import types

def migrate_db():
    migration = Migrate()

    migration.add_column(table_name="contacts", column_name="external_id", data_type=types.VARCHAR(255))
    migration.add_column(table_name="contacts", column_name="data_owners", data_type=types.TEXT())
    default_data_owners = '[]'
    migration.execute("Set default data_owners",
                      f"UPDATE `contacts` set data_owners = '{default_data_owners}' where data_owners is null;")
    migration.add_column(table_name="contacts", column_name="parent_ids", data_type=types.TEXT())
    default_parent_ids = '[]'
    migration.execute("Set default parent_ids",
                      f"UPDATE `contacts` set parent_ids = '{default_parent_ids}' where parent_ids is null;")
    migration.add_column(table_name="contacts", column_name="child_ids", data_type=types.TEXT())
    default_child_ids = '[]'
    migration.execute("Set default child_ids",
                      f"UPDATE `contacts` set child_ids = '{default_child_ids}' where child_ids is null;")
    migration.execute("Drop contact_configuration unique constraint",
                      f"ALTER TABLE `contact_configuration` DROP INDEX `contact_configuration_id`;")

