import requests
import os

class ApiCalls():
    def __init__(self):
        pass

    def getGroupMembershipsByApikey(self, apikey):
        url = os.getenv("APIGATEWAY_INTERNAL_BASEURL") + "/get_my_groups"

        headers = {
            'apikey': apikey
        }

        response = requests.request("GET", url, headers=headers)

        return response.json().get("groups", {})

    def getLoginByApiKey(self, apikey):
        url = os.getenv("APIGATEWAY_INTERNAL_BASEURL") + f"/get_login_by_apikey?apikey={apikey}"

        response = requests.request("GET", url)

        return response.json().get("login", "")


