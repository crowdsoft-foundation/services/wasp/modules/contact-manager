FROM registry.gitlab.com/crowdsoft-foundation/various/python-base-image:production

COPY src /src

EXPOSE 8000

RUN chmod 777 /src/runApp.sh && chmod +x /src/runApp.sh && chmod 777 /src/testApp.sh && chmod +x /src/testApp.sh
WORKDIR "/src/app"
ENTRYPOINT ["/src/runApp.sh", "-r"]
